##### Variables non sensibles #####

##
# in terraform.tfvars
# scw_access_key = "XXXXX"
# scw_secret_key = "XX-XX-XX-XX-XX"
##

# TF_VAR_scw_access_key in CI
variable "scw_access_key" {}

# TF_VAR_scw_secret_key in CI
variable "scw_secret_key" {}

variable "scw_vars" {
    default = {
        zone = "fr-par-1",
        region = "fr-par",
        organization_id = "73ee8be6-578e-4688-a774-3cde5bac03e7"
    }
}

variable "nodes_vars" {
  description = "Variables about nodes"
  default = {
      nbr = "1",
      type = "DEV1-S",
      # https://api-marketplace.scaleway.com/images?page=1&per_page=100
      image = "centos_7.6"
  }
}

variable "project" {
  default = "tf_scw"
}

variable "ports_rules" {
  default = {
    ssh = {
      action = "accept"
      port = 22
      protocol = "TCP"
    }
    jenkins_http = {
      action = "accept"
      port = 8440
      protocol = "TCP"
    }
    jenkins_https = {
      action = "accept"
      port = 8443
      protocol = "TCP"
    }
    influxdb_http = {
      action = "accept"
      port = 8086
      protocol = "TCP"
    }
    grafana_http = {
      action = "accept"
      port = 3000
      protocol = "TCP"
    }
  }
}
