provider "scaleway" {
  version = "~> 1.14.0"
  access_key = var.scw_access_key
  secret_key = var.scw_secret_key
  organization_id = var.scw_vars.organization_id
  zone       = var.scw_vars.zone
  region     = var.scw_vars.region
}
