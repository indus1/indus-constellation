resource "scaleway_instance_security_group" "tf_firewall" {
  inbound_default_policy = "drop"
  outbound_default_policy = "accept"

  dynamic "inbound_rule" {
    for_each = var.ports_rules

    content {
      action = inbound_rule.value.action
      port = inbound_rule.value.port
      ip_range = "0.0.0.0/0"
      protocol = inbound_rule.value.protocol
    }
  }
}

# https://www.terraform.io/docs/providers/scaleway/r/instance_volume.html
resource "scaleway_instance_volume" "tf_vol" {
  name       = "tf_node_vol"
  size_in_gb = 20
  type       = "b_ssd"
}

# https://www.terraform.io/docs/providers/scaleway/r/instance_server.html
resource "scaleway_instance_server" "tf_nodes" {

  count = var.nodes_vars.nbr
  type = var.nodes_vars.type
  image = var.nodes_vars.image
  name = "tf_node_${count.index}"
  state = "started"

  tags = [ "node_${count.index}", "tf_node_${count.index}" ]

	enable_dynamic_ip = true

  additional_volume_ids = [ "${scaleway_instance_volume.tf_vol.id}" ]

  security_group_id = scaleway_instance_security_group.tf_firewall.id
	cloud_init = file("${path.module}/files/cloud-init.yml")
}

#resource "scaleway_account_ssh_key" "tf_ssh" {
#    name       = "tf_ssh"
#    public_key = file(var.ssh_pub)
#}
