resource "local_file" "ansible_inventory" {
  # Few comments :
  #  * Keys map contains <name>.<environment>, we need to concat this with dns suffix to allow Ansible to resolve FQDN
  #    We already ask for a dns zone, so we will just use a replace with a regex to remove the last '.' character
  #  * We map according to Ansible groups that are defined within the playbook
  #  * We export this inventory using INI format in the directory ansible_inventory/<environment>
  content = templatefile("${path.module}/files/ansible_inventory.tmpl", {
    all_ips = join(",", scaleway_instance_server.tf_nodes.*.public_ip)

    slaves_nbr = length(scaleway_instance_server.tf_nodes) - 1
  })

  filename = "./generated_inventory/hosts.cfg"
}

output "instance_ips" {
  value = "${scaleway_instance_server.tf_nodes}"
}
