# Ces valeurs peuvent etre surchargees dans un fichier (ex: prod.tfvars.json) qui sera donné à la cli via un -var-file

#################################### GENERAL AWS
variable "aws_zone" {
  default = "eu-west-3"
}

variable "aws_ec2_zone" {
  default = "eu-west-3a"
}

variable "environment_tag" {
  type    = string
  default = "Jenkins"
}

variable "ec2_volume_mountpoint" {
  type    = list(string)
  default = [
    "/dev/sdb"
#    "/dev/sdc",
#    "/dev/sdd",
#    "/dev/sde",
  ]
}

#################################### EC2 INSTANCE
############ Master
variable "ec2_master_type" {
  type        = string
  # https://aws.amazon.com/fr/ec2/instance-types/
  default     = "t2.micro"
}

############ Agents
variable "agents_number" {
  type    = number
  default = 1
}

variable "ec2_agent_type" {
  type        = string
  # https://aws.amazon.com/fr/ec2/instance-types/
  default     = "t2.micro"
}


#################################### VOLUMES
############ Master
variable "ec2_master_root_volume" {
  type        = any
  description = "Values about ec2, like type or root volume"

  default = {
    volume_size             = "15" # GiB
    volume_type             = "gp2"
    delete_on_termination   = true
    encrypted               = false
  }
}

variable "master_volume_size" {
  type    = number
  default = 30
}

variable "master_volume_per_instance" {
  type    = number
  default = 1
}

############ Agents
variable "ec2_agent_root_volume" {
  type        = any
  description = "Values about ec2, like type or root volume"

  default = {
    volume_size             = "15" # GiB
    volume_type             = "gp2"
    delete_on_termination   = true
    encrypted               = false
  }
}

variable "agent_volume_per_instance" {
  type    = number
  default = 1
}

variable "agent_volume_size" {
  type    = number
  default = 40
}
