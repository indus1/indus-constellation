############################# ATTENTION #############################
#
# Il faut démonter les volumes des instances, ou éteindre les instances
# pour pouvoir faire un terraform destroy
#
#####################################################################

# Configure the AWS Provider
provider "aws" {
  version = "~> 2.1"
  region  = var.aws_zone
}
