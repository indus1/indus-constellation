resource "local_file" "ansible_inventory" {
  # Few comments :
  #  * Keys map contains <name>.<environment>, we need to concat this with dns suffix to allow Ansible to resolve FQDN
  #    We already ask for a dns zone, so we will just use a replace with a regex to remove the last '.' character
  #  * We map according to Ansible groups that are defined within the playbook
  #  * We export this inventory using INI format in the directory ansible_inventory/<environment>
  content               = templatefile("${path.module}/templates/jks_ansible_inventory.yml.tmpl", {
    master_ip = aws_instance.jks_master.public_ip
    
    agents_ips = join(",", aws_instance.jks_agent.*.public_ip)
    agent_nbr = length(aws_instance.jks_agent)  # peut mieux faire avec des variables
  })
  file_permission       = "0660"
  directory_permission  = "0770"
  filename              = "./generated_inventory/hosts.cfg"
}
