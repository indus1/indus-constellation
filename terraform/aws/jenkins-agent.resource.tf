################ Instances
resource "aws_instance" "jks_agent" {
  count = var.agents_number

  ami               = data.aws_ami.latest_centos.id
  instance_type     = var.ec2_agent_type
  availability_zone = var.aws_ec2_zone

  # ssh
  key_name      = "laptop_msi_ubuntu_rsa"

  # cloud-init
  user_data_base64 = data.template_cloudinit_config.config.rendered

  # OS volume
  root_block_device {
    volume_size           = var.ec2_agent_root_volume.volume_size
    volume_type           = var.ec2_agent_root_volume.volume_type
    delete_on_termination = var.ec2_agent_root_volume.delete_on_termination
    encrypted             = var.ec2_agent_root_volume.encrypted
  }

  tags = {
    Name  = "${var.environment_tag}-${count.index}"
    Environment = var.environment_tag
  }

  credit_specification {
    cpu_credits = "standard"
  }
}

################ Volumes
resource "aws_ebs_volume" "ebs_agent_volume" {
  count             = var.agents_number * var.agent_volume_per_instance

  availability_zone = var.aws_ec2_zone
  size              = var.agent_volume_size
  type              = "gp2"

  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_volume_attachment" "ebs_agent_att" {
  count       = var.agents_number * var.agent_volume_per_instance

  device_name = element(var.ec2_volume_mountpoint, count.index)
  volume_id   = aws_ebs_volume.ebs_agent_volume.*.id[count.index]
  instance_id = element(aws_instance.jks_agent.*.id, count.index)
}
