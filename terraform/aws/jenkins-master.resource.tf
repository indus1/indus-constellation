## TODO: network group / security zone / le truc qui fait parefeu et subnet ips
################ Instances
resource "aws_instance" "jks_master" {
  ami               = data.aws_ami.latest_centos.id
  instance_type     = var.ec2_agent_type
  availability_zone = var.aws_ec2_zone

  # ssh
  key_name      = "laptop_msi_ubuntu_rsa"

  # cloud-init
  user_data_base64 = data.template_cloudinit_config.config.rendered

  # OS volume
  root_block_device {
    volume_size           = var.ec2_master_root_volume.volume_size
    volume_type           = var.ec2_master_root_volume.volume_type
    delete_on_termination = var.ec2_master_root_volume.delete_on_termination
    encrypted             = var.ec2_master_root_volume.encrypted
  }

  tags = {
    Name  = "${var.environment_tag}-master"
    Environment = "${var.environment_tag}-master"
  }

  credit_specification {
    cpu_credits = "standard"
  }
}

################ Volumes
resource "aws_ebs_volume" "ebs_master_volume" {
  count             = var.master_volume_per_instance

  availability_zone = var.aws_ec2_zone
  size              = var.master_volume_size
  type              = "gp2"

  tags = {
    Environment = "${var.environment_tag}-master"
  }
}

resource "aws_volume_attachment" "ebs_master_att" {
  count       = var.master_volume_per_instance

  device_name = element(var.ec2_volume_mountpoint, count.index)
  volume_id   = aws_ebs_volume.ebs_master_volume.*.id[count.index]
  instance_id = aws_instance.jks_master.id
}
