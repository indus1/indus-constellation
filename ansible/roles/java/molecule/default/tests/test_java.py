import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')

wanted_keys = ['JAVA_HOME', 'M2_HOME', 'MAVEN_HOME', 'PATH']
java_package = 'adoptopenjdk-11-hotspot'
java_version = '11'
java_vendor = 'AdoptOpenJDK'
maven_version = "3.6.3"


def test_java_maven_env_file(host):
    env_file = host.file('/etc/profile.d/maven.sh')
    assert env_file.exists
    for key in wanted_keys:
        assert env_file.contains(key)


def test_java_maven_env_vars(host):
    cmd = host.run('source /etc/profile.d/maven.sh && printenv')
    assert cmd.rc == 0
    for key in wanted_keys:
        assert key in cmd.stdout


def test_java_package(host):
    pkg = host.package(java_package)
    assert pkg.is_installed
    assert pkg.version.startswith(java_version)


def test_java_version_works(host):
    cmd = host.run('java --version')
    assert cmd.rc == 0
    assert java_version in cmd.stdout
    assert java_vendor in cmd.stdout


def test_maven_version_works(host):
    cmd = host.run('source /etc/profile.d/maven.sh && mvn --version')
    assert cmd.rc == 0
    assert maven_version in cmd.stdout
    assert java_vendor in cmd.stdout
