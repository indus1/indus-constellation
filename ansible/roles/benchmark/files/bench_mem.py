#!/usr/bin/env python3

# sysbench memory --num-threads=140 --memory-oper=write --memory-total-size=50G run <-- op par seconds + MiB/sec
# sysbench memory --num-threads=140 --memory-oper=read --memory-total-size=50G run  <-- op par seconds + MiB/sec

import subprocess           # for executing script
import sys                  # needed for file io
import json
import re

# TODO: chopper la latence ?

def _mem_bench(memory_oper='read'):  # ou 'write'
    operationPerSec = -1
    bandWith = -1

    command = "sysbench memory --num-threads=4 --memory-oper=" + \
        memory_oper + " --memory-total-size=100G run"
    all_info = subprocess.check_output(command, shell=True).decode().strip()
    for line in all_info.split("\n"):
        if "MiB/sec" in line:
            bandWith = re.compile(
                "([0-9]+.[0-9]+) MiB/sec").search(line).group().split()[0]
        if "per second" in line:
            operationPerSec = re.compile(
                "([0-9]+.[0-9]+) per second").search(line).group().split()[0]

    return {
        'bandwith': bandWith,
        'bandwith_unit': 'MiB/sec',
        'operationPerSec': operationPerSec
    }


def mem_info(bench_dir_results):
    mem = _mem_bench()

    with open(bench_dir_results + '/mem.json', 'w') as out:
        json.dump(mem, out)

    return mem


if __name__ == "__main__":
    mem_info("/tmp")
