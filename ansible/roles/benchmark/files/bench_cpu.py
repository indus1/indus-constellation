#!/usr/bin/env python3
import json
import subprocess           # for executing script
import multiprocessing      # to define # of threads


def _sysbench_cpu(threads=1, time=30, max_prime=10000):
    sysbench_threads = " --threads=" + str(threads)
    sysbench_test = " cpu"
    sysbench_max_prime = " --cpu-max-prime=" + str(max_prime)
    sysbench_time = " --time=" + str(time)
    sysbench_cmd = " run"
    prettier = " | grep 'events per second' | awk '{print $4}'"

    cpu_eps = subprocess.check_output(
        'sysbench' + sysbench_threads + sysbench_test +
        sysbench_max_prime + sysbench_time + sysbench_cmd + prettier, shell=True)

    return cpu_eps.decode('UTF-8').rstrip()


def cpu_info(bench_dir_results):
    cpu = {}
    cpu['one_core'] = _sysbench_cpu(1)
    cpu['all_cores'] = _sysbench_cpu(multiprocessing.cpu_count())

    with open(bench_dir_results + '/cpu.json', 'w') as out:
        json.dump(cpu, out)

    return cpu

    # https://github.com/roncotton/sysbench-benchmark/blob/master/bench.py

if __name__ == "__main__":
    cpu_info("/tmp")
