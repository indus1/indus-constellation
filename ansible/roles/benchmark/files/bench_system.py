#!/usr/bin/env python3

import platform             # for computerInfo() method
import socket               # for computerInfo() method
import subprocess           # for executing script
import multiprocessing      # to define # of threads
import sys                  # needed for file io
import time                 # time differences
import json
import re
from psutil import virtual_memory


def _system_cpu_name():
    command = "cat /proc/cpuinfo"
    all_info = subprocess.check_output(command, shell=True).decode().strip()
    for line in all_info.split("\n"):
        if "model name" in line:
            return re.sub(".*model name.*:", "", line, 1).strip()
    return ''


def system_info(bench_dir_results):
    system = {}

    system['hostname'] = socket.gethostname()
    system['platform'] = platform.platform()
    system['cpu_type'] = platform.processor()
    system['cpu_count'] = multiprocessing.cpu_count()
    system['cpu_name'] = _system_cpu_name()
    system['memory'] = virtual_memory().total
    system['timestamp'] = time.time()

    system['python3_version'] = '.'.join([str(sys.version_info.major), str(
        sys.version_info.minor), str(sys.version_info.micro)])
    system['sysbench_version'] = subprocess.check_output(
        "sysbench --version | awk '{print $2}'", shell=True).decode('UTF-8').rstrip()
    system['fio_version'] = subprocess.check_output([
        "fio", "--version"]).decode('UTF-8').rstrip()

    with open(bench_dir_results + '/system.json', 'w') as out:
        json.dump(system, out)

    return system


if __name__ == "__main__":
    system_info("/tmp")
