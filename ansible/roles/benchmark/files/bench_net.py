#!/usr/bin/env python3

import json
import psutil

from speedtest import main as speedmain

speedtest_uri_path: ''


def speedtest():
    return json.loads(speedmain())


def net_info(bench_dir_results):
    net = {}

    addrs = psutil.net_if_addrs()
    for interface in addrs:
        address = addrs[interface][0].address
        net[interface] = {
            'ipv4': {
                'address': addrs[interface][0].address or "",
                'netmask': addrs[interface][0].netmask or ""
            }
        }

    net['speed'] = speedtest()

    with open(bench_dir_results + '/network.json', 'w') as out:
        json.dump(net, out)

    return net


if __name__ == "__main__":
    net_info("/tmp")
