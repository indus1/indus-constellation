#!/usr/bin/env python3

import platform             # for computerInfo() method
import socket               # for computerInfo() method
import subprocess           # for executing script
import multiprocessing      # to define # of threads
import sys                  # needed for file io
import os                   # making a directory
import errno                # error
import time                 # time differences
import shutil               # deleting directory
import datetime             # timestamp
import json
import pwd
import time
import math
from psutil import virtual_memory
from pathlib import Path

from bench_system import system_info
from bench_cpu import cpu_info
from bench_net import net_info
from bench_mem import mem_info
from bench_disk import disk_info

mem = {}
network = {}
disk = {}

summary = {}

home_dir = str(Path.home())
bench_dir = home_dir + "/benchmarks"
bench_dir_results = bench_dir + "/results"
tmp_dir = "/tmp/benchmarks"


def _create_dir():
    Path(bench_dir_results).mkdir(parents=True, exist_ok=True)
    Path(tmp_dir).mkdir(parents=True, exist_ok=True)


def prerequis():
    _create_dir()


def main():
    if (pwd.getpwuid(os.getuid()).pw_name != 'root'):
        print('Please run as root')
        exit(1)

    allStartTime = time.time()

    startTime = time.time()
    print('--- Creation du dossier temporaire ' +
          tmp_dir + ' et de resultat ' + bench_dir_results)
    prerequis()
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    startTime = time.time()
    print('--- Lancement de l inspection du systeme')
    summary['system'] = system_info(bench_dir_results)
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    startTime = time.time()
    print('--- Lancement des tests CPU')
    summary['cpu'] = cpu_info(bench_dir_results)
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    startTime = time.time()
    print('--- Lancement des tests reseau')
    summary['network'] = net_info(bench_dir_results)
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    startTime = time.time()
    print('--- Lancement des tests memoire')
    summary['mem'] = mem_info(bench_dir_results)
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    startTime = time.time()
    print('--- Lancement des tests disque')
    summary['disk'] = disk_info(bench_dir_results)
    print(f'--- Fait en {math.ceil(time.time() - startTime)}s \n')

    with open(bench_dir_results + '/summary.json', 'w') as out:
        json.dump(summary, out)
    print('\n\n --- Les résultats sont disponibles dans ' +
          bench_dir_results + f'/summary.json -- Fait en {math.ceil(time.time() - allStartTime)}s')


if __name__ == "__main__":
    main()
