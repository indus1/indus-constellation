#!/usr/bin/env python3
import subprocess           # for executing script
import json
import os
import pwd
import copy
from retrying import retry
# output => stdout
# https://aws.amazon.com/fr/premiumsupport/knowledge-center/ebs-calculate-optimal-io-size/
# 32kb as it is the example for random r/w
# 256kb as it is the max bs size for aws contiguous read
#  --verify=crc32c --output-format=json --filename=/dev/XXX
# direct=1 => no buffer
#
# readwrite=
# 32kb for 'randrw'
# 256kb for 'readwrite'


def _get_disks():
    command = 'lsblk -o name -n -s -l'
    raw_disks = subprocess.check_output(
        command, shell=True).decode('UTF-8').strip().split('\n')
    # pour supprimer les partitions comme sda1, vda2, etc...
    return list(filter(lambda x: len(x) == 3, raw_disks))


def _disk_bench(operation='readwrite', blockSize='256k', device='/dev/XXX', size='2g'):  # ou 'write'

    command = f"fio --name={operation} --readwrite={operation} --blocksize={blockSize} --direct=1 " + \
        f"--ioengine=libaio --iodepth=16 --size={size} --verify=crc32c " + \
        f"--output-format=json --filename={device}"

    jsonOutput = json.loads(subprocess.check_output(
        command, shell=True).decode('UTF-8').strip())

    return {
        'read': {
            'iops': jsonOutput['jobs'][0]['read']['iops'],
            'drop_ios': jsonOutput['jobs'][0]['read']['drop_ios'],
            'io_bytes': jsonOutput['jobs'][0]['read']['io_bytes'],
            'io_kbytes': jsonOutput['jobs'][0]['read']['io_kbytes'],
            'block_size': copy.deepcopy(blockSize),
            'bw_bytes': jsonOutput['jobs'][0]['read']['bw_bytes'],
            'bw': jsonOutput['jobs'][0]['read']['bw'],
        },
        'write': {
            'iops': jsonOutput['jobs'][0]['write']['iops'],
            'drop_ios': jsonOutput['jobs'][0]['write']['drop_ios'],
            'io_bytes': jsonOutput['jobs'][0]['write']['io_bytes'],
            'io_kbytes': jsonOutput['jobs'][0]['write']['io_kbytes'],
            'block_size': copy.deepcopy(blockSize),
            'bw_bytes': jsonOutput['jobs'][0]['write']['bw_bytes'],
            'bw': jsonOutput['jobs'][0]['write']['bw'],
        }
    }


@retry(stop_max_attempt_number=3, wait_random_min=3, wait_random_max=10)
def disk_info(bench_dir_results):
    if (pwd.getpwuid(os.getuid()).pw_name != 'root'):
        print('Please run as root')
        exit(1)

    disk = {}
    availableDisks = _get_disks()

    # readwrite
    disk['readwrite'] = {}
    for d in availableDisks:
        disk['readwrite'][d] = _disk_bench(
            'readwrite', '256k', f"/dev/{d}", '10m')

    # randrw
    disk['randrw'] = {}
    for d in availableDisks:
        disk['randrw'][d] = _disk_bench('randrw', '32k', f"/dev/{d}", '10m')

    with open(bench_dir_results + '/disk.json', 'w') as out:
        json.dump(disk, out)

    return disk


if __name__ == "__main__":
    disk_info("/tmp")
